import keras
from keras import *
import numpy as np


#training_sequence= np.array([0,1,2,3,4,5,6,7,8,9])
training_label = np.array([2,4,8,16,32,64])

training_sequence = [];
for i in range(0, len(training_label)):
    if i <= 0:
        training_sequence.append([i, 1])
    else:
        print(i)
        print(training_label[i-1])
        training_sequence.append([i, training_label[i-1]])

print(training_sequence)
training_sequence = np.array(training_sequence)

predict_set = np.array([[7,128]])

ep=100000



# '''
# training_sequence = np.array([
#     [0,0],
#     [1,1],
#     [2,2],
#     [3,3],
#     [4,4],
#     [5,5],
#     ])
# '''



def build_model():
    model = keras.Sequential([
        layers.Dense(64, input_shape=[2]),
        layers.Dense(128),
        layers.Dense(256),
        layers.Dense(512),
        layers.Dense(1024),
        layers.Dense(2048),
        layers.Dense(1)
        ])
    model.compile(
            loss=losses.mean_squared_error,
            optimizer=keras.optimizers.RMSprop(0.0001),
            metrics=['mse', 'mae']
            )

    return model

model = build_model();
print(model.summary())
model.fit(training_sequence, training_label,epochs=ep)

# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")


print(model.predict(predict_set))
